/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.reportvalidation;

import java.util.List;

/**
 *
 * @author Sabusi
 */
public class ArtistService {
     public List<ArtistReport> getToptenArtistByTotalPrice(){
         ArtistDao artistDao = new ArtistDao();
         return artistDao.getArtistByTotalPrice(10);
     }
     
      public List<ArtistReport> getToptenArtistByTotalPrice(String begin, String end){
         ArtistDao artistDao = new ArtistDao();
         return artistDao.getArtistByTotalPrice(begin, end, 10);
     }
}
